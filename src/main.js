import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router'
import Vuex from 'vuex'


Vue.use(Vuex)
Vue.use(VueRouter)
import router from './router.js';
import store  from './store'

Vue.config.productionTip = false;


window.Store = store;
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
