import '@mdi/font/css/materialdesignicons.css' 
import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
    },
    theme: {
        themes: {
          light: {
            primary: '#38405F',
            secondary: '#3777FF',
            accent: '#8c9eff',
            error: '#C83E4D',
          },
        },
      },
});
