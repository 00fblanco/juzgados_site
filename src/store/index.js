import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

Vue.use(Vuex);


const vuexLocalStorage = new VuexPersist({
    key: 'xxx-fge',
    storage: window.localStorage,
    reducer: (state) => {
        return {
            Usuario: state.Usuario,
        }
    }
})
const store = new Vuex.Store({
    state: {
        alert: {
            show: false,
            text: 'Prueba de error',
            color: 'primary'
        },
        Usuario: {
          ApiKey: '',
          Rol: ''
        },
    },
    mutations: {
        LOGIN(state, payload){
            state.Usuario = {
                ApiKey: payload.UsuarioApiKey,
                Rol: payload.UsuarioRol
            }
        },
        LOGOUT(state){
            state.Usuario = {
                ApiKey: '',
                Rol: ''
            };
            window.location.reload(true);
        },
        SHOW_ALERT(state, payload){
            state.alert = {
                show: true,
                text: payload.text,
                color: payload.color || 'error'
            }
        }
    },
    actions: {
        login(context, payload){
            context.commit('LOGIN', payload);
        },
        logout(context){
            
            context.commit('LOGOUT');
        },
        showAlert(context, payload){
            context.commit('SHOW_ALERT', payload);
        }
    },
    plugins: [vuexLocalStorage.plugin]
  })

  export default store;