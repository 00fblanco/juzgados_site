
import VueRouter from 'vue-router';
//import requests from '../requests';

import Login from './components/Login';
import JefeJuzgados from './components/JefeJuzgados';
import GestionUsuarios from './components/GestionUsuarios';
import FormUsuarios from './components/FormUsuarios';
import GestionCausasPenales from './components/GestionCausasPenales';
import FormCausasPenales from './components/FormCausasPenales';
import FormAddActores from './components/FormAddActores';
import FormAddDelitos from './components/FormAddDelitos';
import FormReportes from './components/FormReportes';

/*
{ 
    path: '/dashboard', component: Dashboard ,
    beforeEnter: validToken 
  },*/

const routes = [
    { path: '/login', component: Login },
    { 
      path: '/JefeJuzgados', component: JefeJuzgados,
      beforeEnter: validToken,
      children: [
        {
          path: '/usuarios/', component: GestionUsuarios,
          
        },
        {  
          path: '/usuarios/:accion/:id', component: FormUsuarios 

        },
        {
          path: '/causas_penales/', component: GestionCausasPenales,
          
        },
        {

          path: '/causas_penales/:id/registrar_actores', component: FormAddActores

        },
        {

          path: '/causas_penales/:id/registrar_delitos/:accion?', component: FormAddDelitos

        },
        { 
           
          path: '/causas_penales/:accion/:id', component: FormCausasPenales 

        },
        { 
           
          path: '/reportes', component: FormReportes 

        },
        

        
      ]
    },
    { 
      path: '/', 
      beforeEnter: redirect
    },
    
    
    
  ]

const router = new VueRouter({
  routes // short for `routes: routes`
})

function redirect(to, from, next ){
  window.console.log('red')
  let usuario = window.Store.state.Usuario;
  if( usuario == undefined){
    next('login');
  }else{
    next('JefeJuzgados');
    if(usuario.Rol.RolDescripcion === 'Jefe de los Juzgados'){
      //next('JefeJuzgados');
    }
    
  }
}
function validToken(to, from, next){
  
  let usuario = window.Store.state.Usuario.ApiKey;
  window.console.log(usuario);
  if( usuario == undefined || usuario == null || usuario == ''){
    next('/login');
  }else{
    next();
  }
}

export default router;