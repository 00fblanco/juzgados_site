import axios from 'axios';
axios.defaults.baseURL = 'http://localhost:3000/';

export default {
    login(UsuarioNick, UsuarioContrasenia){
        return axios.post('login', 
            {UsuarioNick, UsuarioContrasenia}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    listaUsuarios(Usuario){
        return axios.get('usuarios', {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getJuzgados(Usuario){
        return axios.get('catalogos/juzgados', {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getDeptosRoles(Usuario){
        return axios.get('catalogos/deptos_roles', {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getUsuarioById(Usuario, id){
        return axios.get(`usuario/${id}`, {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getCausaPenalById(Usuario,id){
        return axios.get(`causas_penales/${id}`, {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    }
    ,
    getCausasPenales(Usuario){
        return axios.get('causas_penales', {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getCausasPenalesR1(Usuario, fechaIni, fechaFin){
        return axios.get('causas_penales_r1/'+fechaIni+'/'+fechaFin, {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getTiposActor(Usuario){
        return axios.get('catalogos/tipos_actor', {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getCatSentencias(Usuario){
        return axios.get('catalogos/cat_sentencias', {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getCatDelitos(Usuario){
        return axios.get('catalogos/cat_delitos', {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getDelitosDeCausaPenal(Usuario, CausaPenalId){
        return axios.get('causas_penales_delitos/'+CausaPenalId, {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getCatTipoOrden(Usuario){
        return axios.get('catalogos/cat_tipo_orden', {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getCatSituacionJuridica(Usuario){
        return axios.get('catalogos/cat_situacion_juridica', {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    getAgraviados(Usuario, DelitoCausaPenalId){
        return axios.get('causas_penales_agraviados/'+DelitoCausaPenalId, {
            headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    agregaUsuario(Usuario, UsuarioData){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.post('usuarios', 
            {
                'UsuarioNick': UsuarioData.UsuarioNick,
                'UsuarioContrasenia': UsuarioData.UsuarioContrasenia,
                'UsuarioNombre': UsuarioData.UsuarioNombre,
                'UsuarioApPaterno': UsuarioData.UsuarioApPaterno,
                'UsuarioApMaterno': UsuarioData.UsuarioApMaterno, 
                'DepartamentoId': UsuarioData.DepartamentoId,
                'RolId': UsuarioData.RolId
                ,   
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    agregarActorACausaPenal(Usuario, ActorData, CausaPenalId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.post('causa_penal/add_actor', 
            {
                CausaPenalId: CausaPenalId,
                TipoActorId: ActorData.TipoActor,
                ActorDetenido: ActorData.ActorDetenido,
                ActorNombre: ActorData.ActorNombre,
                ActorApPaterno: ActorData.ActorApPaterno,
                ActorApMaterno: ActorData.ActorApMaterno,
                ActorSexo: ActorData.ActorSexo,
                ActorFechaNacimiento: ActorData.ActorFechaNacimiento,
                ActorCalle: ActorData.ActorCalle,
                ActorNumExterior: ActorData.ActorNumExterior,
                ActorNumInterior: ActorData.ActorNumInterior,
                ActorLatitud: ActorData.ActorLatitud,
                ActorLongitud: ActorData.ActorLongitud,
                delitos: ActorData.delitos.map(x => x.value)
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    agregarAgraviado(Usuario, ActorData, DelitoCausaPenalId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.post('causa_penal/add_agraviado', 
            {
                DelitoCausaPenalId: DelitoCausaPenalId,
                ActorNombre: ActorData.ActorNombre,
                ActorApPaterno: ActorData.ActorApPaterno,
                ActorApMaterno: ActorData.ActorApMaterno,
                ActorSexo: ActorData.ActorSexo,
                ActorFechaNacimiento: ActorData.ActorFechaNacimiento,
                ActorCalle: ActorData.ActorCalle,
                ActorNumExterior: ActorData.ActorNumExterior,
                ActorNumInterior: ActorData.ActorNumInterior,
                ActorLatitud: ActorData.ActorLatitud,
                ActorLongitud: ActorData.ActorLongitud,
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    guardarDelito(Usuario, Delito, DelitoCausaPenalId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('delito', 
            {
                DelitoCausaPenalId: DelitoCausaPenalId,
                incidentes : Delito.Incidentes,
                motivo : Delito.Motivo,
                fechaStcJuridica : Delito.FechaSituacionJuridica,
                SituacionJuridicaId :Delito.SituacionJuridicaId,   
                TipoOrdenId : Delito.TipoOrdenId
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    guardarActor(Usuario, Actor, ActorId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('/causa_penal/update_actor', 
            {
                ...Actor,
                ActorId
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    updateSentencia(Usuario, Sentencia, DelitoCausaPenalId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('delito/update_sentencia', 
            {
                DelitoCausaPenalId: DelitoCausaPenalId,
                CatSentenciaId: Sentencia.CatSentenciaId,
                SentenciaFecha: Sentencia.SentenciaFecha,
                SentenciaPenalidad: Sentencia.SentenciaPenalidad,
                SentenciaReparacionDanio: Sentencia.SentenciaReparacionDanio,
                SentenciaInhabilitacion: Sentencia.SentenciaInhabilitacion,
                SentenciaTmpInhabilitacion: Sentencia.SentenciaTmpInhabilitacion,   
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    guardarSentencia(Usuario, Sentencia, DelitoCausaPenalId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('delito/add_sentencia', 
            {
                DelitoCausaPenalId: DelitoCausaPenalId,
                CatSentenciaId: Sentencia.CatSentenciaId,
                SentenciaFecha: Sentencia.SentenciaFecha,
                SentenciaPenalidad: Sentencia.SentenciaPenalidad,
                SentenciaReparacionDanio: Sentencia.SentenciaReparacionDanio,
                SentenciaInhabilitacion: Sentencia.SentenciaInhabilitacion,
                SentenciaTmpInhabilitacion: Sentencia.SentenciaTmpInhabilitacion,   
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    updateRecurso(Usuario, Recurso, DelitoCausaPenalId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('delito/update_recurso', 
            {
                DelitoCausaPenalId: DelitoCausaPenalId,
                RecursoOfrecimientoPruebas : Recurso.RecursoOfrecimientoPruebas,
                RecursoCierreAudiencia : Recurso.RecursoCierreAudiencia,
                RecursoApelacion : Recurso.RecursoApelacion,
                RecursoExtensionPenal : Recurso.RecursoExtensionPenal,
                RecursoSobreseimiento : Recurso.RecursoSobreseimiento,
                RecursoObservacion : Recurso.RecursoObservacion,   
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    guardarRecurso(Usuario, Recurso, DelitoCausaPenalId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('delito/add_recurso', 
            {
                DelitoCausaPenalId: DelitoCausaPenalId,
                RecursoOfrecimientoPruebas : Recurso.RecursoOfrecimientoPruebas,
                RecursoCierreAudiencia : Recurso.RecursoCierreAudiencia,
                RecursoApelacion : Recurso.RecursoApelacion,
                RecursoExtensionPenal : Recurso.RecursoExtensionPenal,
                RecursoSobreseimiento : Recurso.RecursoSobreseimiento,
                RecursoObservacion : Recurso.RecursoObservacion,   
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    guardarOda(Usuario, Oda, DelitoCausaPenalId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('delito/add_oda', 
            {
                DelitoCausaPenalId: DelitoCausaPenalId,
                TipoOrdenId: Oda.TipoOrdenId,
                OdaFechaOrdenLibrada: Oda.OdaFechaOrdenLibrada,
                OdaNumOfcOrdenLibrada: Oda.OdaNumOfcOrdenLibrada,
                OdaFechaTranscripcionOrden: Oda.OdaFechaTranscripcionOrden,
                OdaNumOfcTranscripcionOrden: Oda.OdaNumOfcTranscripcionOrden,

                LugarCalle: Oda.LugarCalle,
                LugarNumInt: Oda.LugarNumInt,
                LugarNumExt: Oda.LugarNumExt,
                LugarLatitud: Oda.LugarLatitud,
                LugarLongitud: Oda.LugarLongitud,
                LugarTramoCarretero: Oda.LugarTramoCarretero,
                LugarNombreAgente: Oda.LugarNombreAgente,
                LugarDescripcionCaptura: Oda.LugarDescripcionCaptura,    
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    updateOda(Usuario, Oda, DelitoCausaPenalId){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('delito/update_oda', 
            {
                DelitoCausaPenalId: DelitoCausaPenalId,
                TipoOrdenId: Oda.TipoOrdenId,
                OdaFechaOrdenLibrada: Oda.OdaFechaOrdenLibrada,
                OdaNumOfcOrdenLibrada: Oda.OdaNumOfcOrdenLibrada,
                OdaFechaTranscripcionOrden: Oda.OdaFechaTranscripcionOrden,
                OdaNumOfcTranscripcionOrden: Oda.OdaNumOfcTranscripcionOrden,

                LugarCalle: Oda.LugarCalle,
                LugarNumInt: Oda.LugarNumInt,
                LugarNumExt: Oda.LugarNumExt,
                LugarLatitud: Oda.LugarLatitud,
                LugarLongitud: Oda.LugarLongitud,
                LugarTramoCarretero: Oda.LugarTramoCarretero,
                LugarNombreAgente: Oda.LugarNombreAgente,
                LugarDescripcionCaptura: Oda.LugarDescripcionCaptura,    
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    actualizaCausaPenal(Usuario, data){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('causas_penales', 
            {
                'CausaPenalId': data.CausaPenalId,
                'CausaPenalAveriguacionPrevia': data.CausaPenalAveriguacionPrevia,
                'CausaPenalPedimentoPenal': data.CausaPenalPedimentoPenal,
                'CausaPenalDescripcion': data.CausaPenalDescripcion,
                'CausaPenalDetenido': data.CausaPenalDetenido,
                'JuzgadoId': data.JuzgadoId, 
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    actualizarUsuario(Usuario, UsuarioData){
        //axios.defaults.headers = {'Authorization': `bearer ${Usuario.ApiKey}`}
        return axios.put('usuarios', 
            {
                'UsuarioIdd': UsuarioData.UsuarioId,
                'UsuarioNick': UsuarioData.UsuarioNick,
                'UsuarioEstatus': UsuarioData.UsuarioEstatus,
                'UsuarioContrasenia': UsuarioData.UsuarioContrasenia,
                'UsuarioNombre': UsuarioData.UsuarioNombre,
                'UsuarioApPaterno': UsuarioData.UsuarioApPaterno,
                'UsuarioApMaterno': UsuarioData.UsuarioApMaterno, 
                'DepartamentoId': UsuarioData.DepartamentoId,
                'RolId': UsuarioData.RolId
                ,   
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    agregarCausaPenal(Usuario, data){
        return axios.post('causas_penales', 
            {
                'CausaPenalAveriguacionPrevia': data.CausaPenalAveriguacionPrevia,
                'CausaPenalPedimentoPenal': data.CausaPenalPedimentoPenal,
                'CausaPenalDescripcion': data.CausaPenalDescripcion,
                'CausaPenalDetenido': data.CausaPenalDetenido,
                'JuzgadoId': data.JuzgadoId
            }, { headers: {'Authorization': "Bearer "+Usuario.ApiKey } }
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    CausaPenalSetEstatus(Usuario,CausaPenalId, CausaPenalEstatus){
        return axios.put(`/causas_penales_estatus/`, 
            {
                CausaPenalId, CausaPenalEstatus,},
            { headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    },
    usuarioSetEstatus(Usuario,UsuarioId, UsuarioEstatus){
        return axios.post(`/usuario_estatus/`, 
            {
                _UsuarioId: UsuarioId, UsuarioEstatus
            },
            { headers: {'Authorization': "Bearer "+Usuario.ApiKey }}
        ).then(data => {
            return data;
        }).catch(err => {
            return err;
        })
    }
}